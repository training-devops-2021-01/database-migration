# Database Migration dengan Docker Image #

[![Konsep Database Migration](./img/database-migration.jpg)](./img/database-migration.jpg)

[![Database Replication](./img/database-replication.jpg)](./img/database-replication.jpg)

1. Script migration ditulis di dalam folder `sql` dengan nama file sesuai [aturan penamaan Flyway](https://gitlab.com/training-devops-2021-01/database-migration)

2. Setiap ada perubahan database, buat file baru dalam folder `sql`, kemudian rebuild image

    ```
    docker build -t endymuhardin/database-migration .
    ```

3. Buat tag baru sesuai [Semantic Versioning](https://semver.org/) atau [Calendar Versioning](https://calver.org/)

    ```
    docker tag endymuhardin/database-migration endymuhardin/database-migration:2021031801
    ```

4. Push ke Docker Hub

    ```
    docker push endymuhardin/database-migration:latest
    docker push endymuhardin/database-migration:2021031801
    ```

# Otomasi Deployment dengan Gitlab CI #

1. Copy konfigurasi kubernetes di komputer local yang sudah dites dan sukses

    ```
    cat kubeconfig | base64  
    ```

    Copy nilai yang tampil. Atau gunakan perintah berikut supaya langsung masuk ke clipboard

    ```
    cat kubeconfig | base64 | pbcopy 
    ```

2. Buat variabel `KUBECONFIGFILE` berisi hasil copy di langkah sebelumnya

3. Buat commit baru, kemudian tag dengan nama tag sesuai dengan regular expression `[a-z0-9]([-a-z0-9]*[a-z0-9])?(\.[a-z0-9]([-a-z0-9]*[a-z0-9])?)*`

4. Push berikut tagnya

    ```
    git push --follow-tags
    ```