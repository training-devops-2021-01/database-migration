create table antrian (
    id INT PRIMARY KEY AUTO_INCREMENT,
    email varchar(255) not null,
    waktu_pendaftaran datetime not null,
    urutan INT not null
);

alter table antrian add unique(email);